package com.homeplus.services;

import com.homeplus.dtos.comment.CommentGetDto;
import com.homeplus.dtos.comment.CommentPostDto;
import com.homeplus.dtos.comment.CommentPutDto;
import com.homeplus.models.CommentEntity;
import com.homeplus.models.TaskEntity;
import com.homeplus.repositories.CommentRepository;
import com.homeplus.utility.mapper.CommentMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CommentService {

    private final CommentRepository commentRepository;
    private final CommentMapper commentMapper;
    private final TaskService taskService;
    private final UserService userService;

    public List<CommentGetDto> getCommentsByTaskId(Long id) {
        TaskEntity task = taskService.getTaskEntityById(id);
        return commentRepository.findCommentsByTask(task).stream()
                .map(comment -> commentMapper.fromEntity(comment))
                .collect(Collectors.toList());
    }

    public void createComment(CommentPostDto commentPostDto) {
        commentPostDto.setUserEntity(
                userService.getUserById(commentPostDto.getUser_id())
        );
        commentPostDto.setTaskEntity(
                taskService.getTaskEntityById(commentPostDto.getTask_id())
        );
        CommentEntity commentEntity = commentMapper.postDtoToEntity(commentPostDto);
        commentRepository.save(commentEntity);
    }

    public void deleteComment(Long id) {
        boolean commentExist = commentRepository.findById(id).isPresent();
        if (commentExist) {
            commentRepository.deleteById(id);
        } else {
            throw new IllegalStateException("comment is not exist");
        }
    }


    public void updateComment(CommentPutDto commentPutDto) {
        boolean commentExist = commentRepository.findById(commentPutDto.getId()).isPresent();
        if (commentExist) {
            commentRepository.save(commentMapper.putDtoToEntity(commentPutDto));
        } else {
            throw new IllegalStateException("comment is not exist");
        }
    }
}
