package com.homeplus.dtos.tasker;

import com.homeplus.models.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TaskerPutDto {

    private Long id;

    private UserEntity userEntity;

    private String title;

    private String skills_description;

    private String certifications;

    private String category;

    private String bank_bsb;

    private String bank_account;

    private OffsetDateTime created_time;

    private final OffsetDateTime updated_time = OffsetDateTime.now();
}
