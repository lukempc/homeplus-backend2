package com.homeplus.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Getter
@Setter
@Entity
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Table(name = "task_post_offer")
public class TaskOfferEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "offer_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "tasker_id", referencedColumnName = "tasker_id")
    private TaskerEntity taskerEntity;

    @ManyToOne
    @JoinColumn(name = "task_id", referencedColumnName = "task_id")
    private TaskEntity taskEntity;

    @Column(name = "offered_price", nullable = false)
    private String offered_price;

    @Column(name = "offer_status", nullable = false)
    @Enumerated(EnumType.STRING)
    private TaskOfferStatus offer_status;

    @Column(name = "description")
    private String description;

    @Column(name = "reply_message")
    private String reply_message;

    @Column(name = "created_time", nullable = false)
    private OffsetDateTime created_time;

    @Column(name = "updated_time", nullable = false)
    private OffsetDateTime updated_time;
}
