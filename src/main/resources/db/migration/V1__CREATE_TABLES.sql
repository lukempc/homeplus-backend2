CREATE SCHEMA if not exists "homeplus";

DROP TABLE IF EXISTS "time_sections";
CREATE TABLE "time_sections" (
    "time_section_id" SERIAL PRIMARY KEY,
    "title" VARCHAR(255) NOT NULL,
    "description" VARCHAR(255) NOT NULL
);

DROP TABLE IF EXISTS "user_info";
CREATE TABLE "user_info" (
    "user_id" SERIAL PRIMARY KEY,
    "name" VARCHAR(255) NOT NULL,
    "email" VARCHAR(255) UNIQUE NOT NULL,
    "password" CHAR(64) NOT NULL,
    "gender" CHAR(1),
    "language" VARCHAR(255),
    "state" VARCHAR(255),
    "street" VARCHAR(255),
    "postcode" VARCHAR(255),
    "date_of_birth" DATE,
    "mobile" VARCHAR(10),
    "is_tasker" BOOL DEFAULT FALSE,
    "created_time" TIMESTAMP WITH TIME ZONE NOT NULL,
    "updated_time" TIMESTAMP WITH TIME ZONE NOT NULL
);

DROP TABLE IF EXISTS "tasker_info";
CREATE TABLE "tasker_info" (
    "tasker_id" SERIAL PRIMARY KEY,
    "user_id" INT REFERENCES "user_info" ("user_id"),
    "title" VARCHAR(255) NOT NULL,
    "skills_description" VARCHAR(255) NOT NULL,
    "certifications" VARCHAR(255),
    "category" VARCHAR(255),
    "bank_bsb" VARCHAR(255) NOT NULL,
    "bank_account" VARCHAR(255) NOT NULL,
    "created_time" TIMESTAMP WITH TIME ZONE NOT NULL,
    "updated_time" TIMESTAMP WITH TIME ZONE NOT NULL
);

DROP TABLE IF EXISTS "task_post";
CREATE TABLE "task_post" (
    "task_id" SERIAL PRIMARY KEY,
    "user_id" INT REFERENCES "user_info" ("user_id"),
    "tasker_id" INT REFERENCES "tasker_info" ("tasker_id"),
    "title" VARCHAR(255) NOT NULL,
    "category" VARCHAR(255),
    "date" DATE,
    "time_section" INT REFERENCES "time_sections" ("time_section_id"),
    "budget" VARCHAR(255),
    "state" VARCHAR(255),
    "street" VARCHAR(255),
    "suburb" VARCHAR(255),
    "postcode" VARCHAR(255),
    "house_type" VARCHAR(255),
    "num_of_rooms" VARCHAR(255),
    "num_of_bathrooms" VARCHAR(255),
    "levels" VARCHAR(255),
    "lift" BOOL,
    "in_person" BOOL,
    "item_name" VARCHAR(255),
    "item_value" VARCHAR(255),
    "description" VARCHAR(255),
    "review" VARCHAR(255),
    "rating" VARCHAR(255),
    "task_status" VARCHAR(255),
    "created_time" TIMESTAMP WITH TIME ZONE NOT NULL,
    "updated_time" TIMESTAMP WITH TIME ZONE NOT NULL,
    CONSTRAINT "check_task_status" CHECK ("task_status" in ('open', 'assigned', 'completed', 'aborted'))
);

DROP TABLE IF EXISTS "task_post_offer";
CREATE TABLE "task_post_offer" (
    "offer_id" SERIAL PRIMARY KEY,
    "task_id" INT REFERENCES "task_post" ("task_id"),
    "tasker_id" INT REFERENCES "tasker_info" ("tasker_id"),
    "offered_price" VARCHAR(255),
    "offer_status" VARCHAR(255),
    "description" VARCHAR(255),
    "reply_message" VARCHAR(255),
    "created_time" TIMESTAMP WITH TIME ZONE NOT NULL,
    "updated_time" TIMESTAMP WITH TIME ZONE NOT NULL,
    CONSTRAINT "check_offer_status" CHECK ("offer_status" in ('PENDING', 'ACCEPTED', 'REJECTED', 'CANCELED'))
);

DROP TABLE IF EXISTS "task_post_comment";
CREATE TABLE "task_post_comment" (
    "comment_id" SERIAL PRIMARY KEY,
    "task_id" INT REFERENCES "task_post" ("task_id"),
    "user_id" INT REFERENCES "user_info" ("user_id"),
    "message" VARCHAR(255),
    "created_time" TIMESTAMP WITH TIME ZONE NOT NULL,
    "updated_time" TIMESTAMP WITH TIME ZONE NOT NULL
);



